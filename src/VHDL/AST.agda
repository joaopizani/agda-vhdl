open import Data.String using (String)
open import Data.List using (List; _∷_) renaming ([] to ε)
open import Data.Char using (Char)
open import Data.Nat using (ℕ)
open import Data.Maybe using (Maybe)


specialChars : List Char
specialChars =   '"' ∷ '#' ∷ '&' ∷ '\'' ∷ '(' ∷ ')' ∷ '*' ∷ '+' ∷ ',' ∷ '-'
               ∷ '.' ∷ '/' ∷ ':' ∷ ';' ∷ '<' ∷ '=' ∷ '>' ∷ '_' ∷ '|' ∷ ε 

otherSpecialChars : List Char
otherSpecialChars =   '!' ∷ '$' ∷ '%' ∷ '@' ∷ '?' ∷ '[' ∷ '\\'
                    ∷ ']' ∷ '^' ∷ '`' ∷ '{' ∷ '}' ∷ '~' ∷ ε 


data Ident : Set where
    BasicIdent    : String → Ident
    ExtendedIdent : String → Ident

fromIdent : Ident → String
fromIdent (BasicIdent s)    = s
fromIdent (ExtendedIdent s) = s

unsafeBasicIdent : String → Ident
unsafeBasicIdent = BasicIdent

unsafeExtendedIdent : String → Ident
unsafeExtendedIdent = ExtendedIdent


SimpleName = Ident

data Expr : Set

data Choice : Set where
    ChoiceExpr : Expr → Choice
    Others     : Choice

record ElemAssoc : Set where
    inductive
    field choice : Maybe Choice
          expr   : Expr

FormalPart = SimpleName

ActualPart : Set

record AssocElem : Set where
    inductive
    field formal : Maybe FormalPart
          actual : ActualPart

data Name : Set

data ActualDesig : Set where
    ADName : Name → ActualDesig
    ADExpr : Expr → ActualDesig
    Open   : ActualDesig

ActualPart = ActualDesig

Prefix : Set

record IndexedName : Set where
    inductive
    field pr    : Prefix
          exprs : List Expr

data Suffix : Set where
    SSimple : SimpleName → Suffix
    All     : Suffix

record SelectedName : Set where
    inductive
    field prf : Prefix
          suf : Suffix

record AttribName : Set where
    inductive
    field
        pr   : Prefix
        name : Name
        expr : Maybe Expr

data Range : Set where
    AttribRange                    : AttribName  → Range
    SubtypeRange ToRange DownRange : Expr → Expr → Range

DiscreteRange = Range

record SliceName : Set where
    inductive
    field pr    : Prefix
          range : DiscreteRange

data Name where
    NSimple    : SimpleName   → Name
    NSelected  : SelectedName → Name
    NIndexed   : IndexedName  → Name
    NSlice     : SliceName    → Name
    NAttribute : AttribName   → Name

Prefix = Name

Literal = String

record FCall : Set where
    inductive
    field fname : Name
          args  : List AssocElem

data Expr where
    And Or Xor Nand Nor Xnor Sll Srl Sla Sra Rol Ror Mod Rem : Expr → Expr → Expr
    _V≡_ _V≢_ _V<_ _V≤_ _V>_ _V≥_  : Expr → Expr → Expr
    _V+_ _V-_ _V&_ _V*_ _V/_ _V**_ : Expr → Expr → Expr
    Neg Pos Abs Not : Expr → Expr
    Aggregate : List ElemAssoc → Expr
    PrimName  : Name           → Expr
    PrimLit   : Literal        → Expr
    PrimFCall : FCall          → Expr

logicalPrec relationalPrec shiftPrec plusPrec signPrec multPrec miscPrec : ℕ
logicalPrec    = 2
relationalPrec = 3
shiftPrec      = 4
plusPrec       = 5
signPrec       = 6
multPrec       = 7
miscPrec       = 8



data Mode : Set where
    In  : Mode
    Out : Mode

TypeMark = SimpleName

record SigDecl : Set where
    field
        ident    : Ident
        mode     : Mode
        typeMark : TypeMark

record EntityDecl : Set where
    field ident    : Ident
          sigDecls : List SigDecl

record IFaceVarDecl : Set where
    field ident    : Ident
          typeMark : TypeMark

record SubProgSpec : Set where
    field
        ident    : Ident
        vardecls : List IFaceVarDecl
        typeMark : TypeMark

record WformElem : Set where
    field expr  : Expr
          after : Maybe Expr

data Wform : Set where
    WformCon   : List WformElem → Wform
    Unaffected : Wform

data SeqStmt : Set

record ElseIf : Set where
    inductive
    field cond : Expr
          body : List SeqStmt

record Else : Set where
    inductive
    field body : List SeqStmt

record CaseStmtAlt : Set where
    inductive
    field choices : List Choice
          body    : List SeqStmt

data SeqStmt where
    IfStmt     : Expr → List SeqStmt → List ElseIf → Maybe Else → SeqStmt
    CaseStmt   : Expr → List CaseStmtAlt                        → SeqStmt
    ReturnStmt : Maybe Expr                                     → SeqStmt
    ForStmt    : Ident → DiscreteRange → List SeqStmt           → SeqStmt
    AssignStmt : Name → Expr                                    → SeqStmt
    WaitFor    : Expr                                           → SeqStmt
    SigAssing  : Name → Wform                                   → SeqStmt
    ProcCall   : Name → List AssocElem                          → SeqStmt


record IndexConstraint : Set where
    field ranges : List DiscreteRange

Constraint = IndexConstraint

data SubtypeConstraint : Set where
    ConstraintIndex : Constraint → SubtypeConstraint
    ConstraintRange : Range → SubtypeConstraint

record SubtypeInd : Set where
    field typeMark   : TypeMark
          constraint : Maybe SubtypeConstraint

record SubtypeDecl : Set where
    field ident : Ident
          ind   : SubtypeInd

record VarDecl : Set where
    field
        ident : Ident
        subty : SubtypeInd
        expr  : Maybe Expr

record ConstDecl : Set where
    field
        ident : Ident
        subty : SubtypeInd
        expr  : Maybe Expr

record SubProgBody : Set

data SubProgDeclItem : Set where
    SPVD : VarDecl     → SubProgDeclItem
    SPCD : ConstDecl   → SubProgDeclItem
    SPSB : SubProgBody → SubProgDeclItem

record SubProgBody where
    inductive
    field
        spec  : SubProgSpec
        items : List SubProgDeclItem
        stmts : List SeqStmt

data BlkDeclItem : Set where
    BDISPB : SubProgBody → BlkDeclItem
    BDISD  : SigDecl     → BlkDeclItem


Label = Ident

record BlaDecl : Set where
    field
        ident    : Ident
        typeMark : TypeMark
        expr     : Maybe Expr

record PMapAspect : Set where
    field elems : List AssocElem

record When : Set where
    field expr : Expr

record WhenElse : Set where
    field wform : Wform

record CondSigAssignBody : Set where
    field
        main  : Wform
        when  : Maybe When
        elses : List WhenElse

record CondSigAssignStmt : Set where
    field target  : Name
          consigs : CondSigAssignBody

record InsUnit : Set where
    field entityName : Name

record CompInsStmt : Set where
    field
        label      : Label
        insUnit    : InsUnit
        pMapAspect : PMapAspect

record ProcStmt : Set where
    field
        label : Label
        names : List SimpleName
        body  : List SeqStmt

data GenStmt : Set where
    ForGen : Ident → DiscreteRange → GenStmt
    IfGen  : Expr                  → GenStmt

record IFaceSigDecl : Set where
    field
        ident    : Ident
        mode     : Mode
        typeMark : TypeMark

data ConcStmt : Set

record BlkStmt : Set where
    inductive
    field
        label        : Label
        sigDecls     : List IFaceSigDecl
        pMapAspect   : PMapAspect
        blkDeclItems : List BlkDeclItem
        body         : List ConcStmt

record GenerateStmt : Set where
    inductive
    field
        label    : Label
        genStmt  : GenStmt
        blkItems : List BlkDeclItem
        body     : List ConcStmt

data ConcStmt where
    CSBStmt  : BlkStmt           → ConcStmt
    CSSAStmt : CondSigAssignStmt → ConcStmt
    CSIStmt  : CompInsStmt       → ConcStmt
    CSPSm    : ProcStmt          → ConcStmt
    CSGStmt  : GenerateStmt      → ConcStmt


record ArchBody : Set where
    field
        ident     : Ident
        name      : Name
        blkDecls  : List BlkDeclItem
        concStmts : List ConcStmt

data ArrayTypeDef : Set where
    UnconsArrayDef : List TypeMark   → TypeMark → ArrayTypeDef
    ConsArrayDef   : IndexConstraint → TypeMark → ArrayTypeDef

record ElementDecl : Set where
    field ident    : Ident
          typeMark : TypeMark

record RecordTypeDef : Set where
    field elems : List ElementDecl

record EnumTypeDef : Set where
    field idents : List Ident

record IntegerTypeDef : Set where
    field range : DiscreteRange

data TypeDef : Set where
    TDA : ArrayTypeDef   → TypeDef
    TDR : RecordTypeDef  → TypeDef
    TDE : EnumTypeDef    → TypeDef
    TDI : IntegerTypeDef → TypeDef

record TypeDecl : Set where
    field ident : Ident
          def   : TypeDef

data PkgDeclItem : Set where
    PDITD : TypeDecl    → PkgDeclItem
    PDISD : SubtypeDecl → PkgDeclItem
    PDISS : SubProgSpec → PkgDeclItem

record PkgDec : Set where
    field ident : Ident
          items : List PkgDeclItem

PkgBodyDeclItem = SubProgBody

record PkgBody : Set where
    field ident : Ident
          items : List PkgBodyDeclItem


data LibUnit : Set where
    LUEntity  : EntityDecl → LibUnit
    LUArch    : ArchBody   → LibUnit
    LUPkgDec  : PkgDec     → LibUnit
    LUPkgBody : PkgBody    → LibUnit

data ContextItem : Set where
    Lib : Ident        → ContextItem
    Use : SelectedName → ContextItem

record DesignFile : Set where
    field contexts : List ContextItem
          libunits : List LibUnit
