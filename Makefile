STDLIB=${HOME}/build/agda/lib/current/src
SRC=src
TRGBYTECODE=$(SRC)
TRGLATEX=latex

all: bytecode tex

bytecode: \
	$(TRGBYTECODE)/VHDL/AST.agdai

tex: \
	$(TRGLATEX)/VHDL/AST.tex


$(TRGBYTECODE)/%.agdai: $(SRC)/%.lagda
	agda -i $(STDLIB) -i $(SRC) $<

$(TRGLATEX)/%.tex: $(SRC)/%.lagda
	agda -i $(STDLIB) -i $(SRC) --latex-dir=$(TRGLATEX) --latex $<

clean:
	find -L $(TRGBYTECODE) -name '*.agdai' -delete
	find -L $(TRGLATEX) -name '*.tex' -delete

.PHONY: clean bytecode tex
